<?php

/**
 * @file
 * Provides integration for converse.js library:
 * An XMPP chat client for your website
 */

/**
 * Library modes.
 */
define('CONVERSEJS_UNPACKED', 1);
define('CONVERSEJS_PACKED', 2);

/**
 * Implements hook_menu().
 */
function conversejs_menu() {
  $items = array();

  $items['admin/config/services/conversejs'] = array(
    'type' => MENU_NORMAL_ITEM,
    'title' => 'Converse.js settings',
    'description' => 'Setup converse.js parameters to connect to your XMPP server.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conversejs_admin_page'),
    'access arguments' => array('administer conversejs settings'),
    'file' => 'conversejs.admin.inc',
  );

  $items['admin/config/services/conversejs/settings'] = array(
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'title' => 'Converse.js settings',
    'weight' => 1,
  );

  $items['admin/config/services/conversejs/advanced'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Advanced settings',
    'description' => 'Advanced parameters for converse.js library.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conversejs_admin_page_advanced'),
    'access arguments' => array('administer conversejs settings'),
    'file' => 'conversejs.admin.inc',
    'weight' => 2,
  );

  $items['admin/config/services/conversejs/libraries'] = array(
    'type' => MENU_LOCAL_TASK,
    'title' => 'Libraries placement',
    'description' => 'Configure converse.js libraries. This is for advanced users only.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('conversejs_admin_page_libraries'),
    'access arguments' => array('administer conversejs settings'),
    'file' => 'conversejs.admin.inc',
    'weight' => 3,
  );

  $items['conversejs/prebind'] = array(
    'type' => MENU_CALLBACK,
    'page callback' => 'conversejs_prebind',
    'access arguments' => array('autologin in conversejs'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function conversejs_permission() {
  return array(
    'administer conversejs settings' => array(
      'title' => t('Administer converse.js settings'),
      'description' => t('Allows the user to configure converse.js parameters'),
    ),

    'use conversejs' => array(
      'title' => t('Use converse.js'),
      'description' => t('Allows the user to use converse.js for chatting.'),
    ),

    'autologin in conversejs' => array(
      'title' => t('Auto login in conversejs'),
      'description' => t('Allows the user to use autologin feature of converse.js. If enabled, the user will be automatically logged in in chat room when page loads.'),
    ),
  );
}

/**
 * Implements hook_page_alter().
 */
function conversejs_page_alter(&$page) {
  if (conversejs_is_enabled()) {
    $page['page_bottom']['conversejs'] = array(
      '#markup' => '<div id="conversejs"></div>',
    );
  }
}

/**
 * Implements hook_init().
 */
function conversejs_init() {
  if (conversejs_is_enabled()) {
    // Add css & js for converse.js
    $path = drupal_get_path('module', 'conversejs');

    // drupal_add_js($path . '/lib/converse.js/builds/converse.min.js');
    // drupal_add_js($path . '/lib/jquery.min.js');
    conversejs_add_javascript_libraries();
    drupal_add_css($path . '/lib/converse.js/converse.css');

    // Add javascript to trigger the converse.js with custom settings.
    drupal_add_js($path . '/conversejs.js');

    // Add css for custom styling.
    drupal_add_css($path . '/conversejs.css');

    // Add converse.js parameters.
    $conversejs_params = conversejs_get_params(NULL, TRUE);

    drupal_add_js(array(
      'conversejs' => array(
        'prebind_path' => url('conversejs/prebind'),
        'bosh_service_url' => conversejs_get_params('conversejs_use_proxy') ? conversejs_get_params('conversejs_proxy_address', TRUE) : conversejs_get_params('conversejs_bosh_server', TRUE),
        'prebind' => (bool) $conversejs_params['conversejs_autologin'],
        'fullname' => $conversejs_params['conversejs_param_fullname'],
        'allow_contact_requests' => (bool) $conversejs_params['conversejs_param_allow_contact_requests'],
        'allow_muc' => (bool) $conversejs_params['conversejs_param_allow_muc'],
        'animate' => (bool) $conversejs_params['conversejs_param_animate'],
        'auto_list_rooms' => (bool) $conversejs_params['conversejs_param_auto_list_rooms'],
        'auto_reconnect' => (bool) $conversejs_params['conversejs_param_auto_reconnect'],
        'auto_subscribe' => (bool) $conversejs_params['conversejs_param_auto_subscribe'],
        'allow_otr' => (bool) $conversejs_params['conversejs_param_allow_otr'],
        'use_otr_by_default' => (bool) $conversejs_params['conversejs_param_use_otr_by_default'],
        'cache_otr_key' => (bool) $conversejs_params['conversejs_param_cache_otr_key'],
        'debug' => (bool) $conversejs_params['conversejs_param_debug'],
        'expose_rid_and_sid' => (bool) $conversejs_params['conversejs_param_expose_rid_and_sid'],
        'hide_muc_server' => (bool) $conversejs_params['conversejs_param_hide_muc_server'],
        'i18n_locale' => $conversejs_params['conversejs_param_i18n_locale'],
        'show_controlbox_by_default' => (bool) $conversejs_params['conversejs_param_show_controlbox_by_default'],
        'show_call_button' => (bool) $conversejs_params['conversejs_param_show_call_button'],
        'show_only_online_users' => (bool) $conversejs_params['conversejs_param_show_only_online_users'],
        'use_vcards' => (bool) $conversejs_params['conversejs_param_use_vcards'],
        'xhr_custom_status' => (bool) $conversejs_params['conversejs_param_xhr_custom_status'],
        'xhr_custom_status_url' => $conversejs_params['conversejs_param_xhr_custom_status_url'],
        'xhr_user_search' => (bool) $conversejs_params['conversejs_param_xhr_user_search'],
        'xhr_user_search_url' => $conversejs_params['conversejs_param_xhr_user_search_url'],
      ),
    ), 'setting');
  }
}

/**
 * Pre-Authenticate a XMPP BOSH session and bind it to converse.js client.
 */
function conversejs_prebind() {
  // We use XmppPrebind library for pre-binding XMPP session.
  require_once dirname(__FILE__) . '/lib/xmpp-prebind-php/lib/XmppPrebind.php';

  $prebind_info = conversejs_get_xmpp_prebind_info();
  $session_info = array();

  try {
    $xmpp_prebind = new XmppPrebind($prebind_info['domain'], $prebind_info['bosh_address'], $prebind_info['resource'], FALSE, FALSE);
    $xmpp_prebind->connect($prebind_info['username'], $prebind_info['password']);
    $xmpp_prebind->auth();
    // Array containing sid, rid and jid.
    $session_info = $xmpp_prebind->getSessionInfo();
  }
  catch (\Exception $e) {
    watchdog_exception('conversejs', $e);
    $session_info['error'] = t('Server-Side authentication failed.');
    $session_info['error'] .= ' ' . t('Error message: @msg', array(
      '@msg' => $e->getMessage(),
    ));
  }

  drupal_json_output($session_info);
  drupal_exit();
}

/**
 * Decide if converse.js should be enabled in current page or not.
 *
 * This check is according to settings and user permissions.
 *
 * @param bool $check_perm
 *   Determines whether we should also check user permissions or not.
 *
 * @return bool
 *   whether converse.js should be enabled on this page.
 */
function conversejs_is_enabled($check_perm = TRUE) {
  $conversejs_visibility = variable_get('conversejs_visibility', 0);
  $conversejs_pages = variable_get('conversejs_pages', "admin\nadmin/*");

  if ($conversejs_visibility < 2) {
    $path = drupal_get_path_alias($_GET['q']);
    // Compare with the internal and path alias (if any).
    $page_match = drupal_match_path($path, $conversejs_pages);
    if ($path != $_GET['q']) {
      $page_match = $page_match || drupal_match_path($_GET['q'], $conversejs_pages);
    }
    // When $conversejs_visibility has a value of 0,
    // the converse.js chat room is displayed on
    // all pages except those listed in $conversejs_pages.
    // When set to 1, it is displayed only on those pages listed
    // in $conversejs_pages.
    $page_match = !($conversejs_visibility xor $page_match);
  }
  else {
    $page_match = php_eval($conversejs_pages);
  }

  return $check_perm ? $page_match && user_access('use conversejs') : $page_match;
}

/**
 * Get XMPP Pre-bind settings and informations.
 *
 * Resolves tokens and returns XMPP credentials as an
 * array in the following format:
 *
 * @code
 * Array(
 *   'username'     => XMPP username,
 *   'domain'       => XMPP domain name,
 *   'resource'     => XMPP resource name,
 *   'full_jid'     => Full jabber id,
 *   'bare_jid'     => Bare jabber id,
 *   'password'     => XMPP Password,
 *   'bosh_address' => BOSH Address,
 * )
 * @endcode
 *
 * If conversejs_autologin is disabled or user does not
 * have access to auto-login, this function returns FALSE.
 */
function conversejs_get_xmpp_prebind_info() {
  $conversejs_autologin = conversejs_get_params('conversejs_autologin', TRUE);
  if (!$conversejs_autologin) {
    return FALSE;
  }

  $conversejs_use_proxy = conversejs_get_params('conversejs_use_proxy', TRUE);
  if ($conversejs_use_proxy) {
    $bosh_address = conversejs_get_params('conversejs_proxy_address', TRUE);
  }
  else {
    $bosh_address = conversejs_get_params('conversejs_bosh_server', TRUE);
  }

  $conversejs_xmpp_user = conversejs_get_params('conversejs_xmpp_user', TRUE);
  $conversejs_xmpp_password = conversejs_get_params('conversejs_xmpp_password', TRUE);
  $conversejs_xmpp_domain = conversejs_get_params('conversejs_xmpp_domain', TRUE);
  $conversejs_xmpp_resource = conversejs_get_params('conversejs_xmpp_resource', TRUE);

  // Append random string to resource name for uniqueness.
  $conversejs_xmpp_resource .= '-' . mt_rand(10000, 99999);

  $info = array(
    'username' => $conversejs_xmpp_user,
    'domain' => $conversejs_xmpp_domain,
    'resource' => $conversejs_xmpp_resource,
    'full_jid' => "{$conversejs_xmpp_user}@{$conversejs_xmpp_domain}/{$conversejs_xmpp_resource}",
    'bare_jid' => "{$conversejs_xmpp_user}@{$conversejs_xmpp_domain}",
    'password' => $conversejs_xmpp_password,
    'bosh_address' => $bosh_address,
  );

  return $info;
}

/**
 * Get converse.js parameters in an array.
 *
 * @param string $param_name
 *   If param name is provided, only this parameter's value will be returned;
 *   otherwise all parameters will be returned in an associative array.
 *   If $param_name is invalid, FALSE is returned.
 * @param bool $prepare
 *   Determines whether we should resolve tokens in parameters or not.
 *
 * @return string
 *   Parameter value.
 */
function conversejs_get_params($param_name = NULL, $prepare = FALSE) {
  global $user;

  $static_name = 'conversejs_get_params' . ($prepare ? '_prepare' : '');
  $params = &drupal_static($static_name);
  if (is_array($params)) {
    if ($param_name) {
      return isset($params[$param_name]) ? $params[$param_name] : FALSE;
    }
    else {
      return $params;
    }
  }

  $params = array(
    'conversejs_bosh_server' => variable_get('conversejs_bosh_server', ''),
    'conversejs_use_proxy' => variable_get('conversejs_use_proxy', 0),
    'conversejs_proxy_address' => variable_get('conversejs_proxy_address', drupal_get_path('module', 'conversejs') . '/http_bind.php'),
    'conversejs_autologin' => variable_get('conversejs_autologin', 0),
    'conversejs_xmpp_user' => variable_get('conversejs_xmpp_user', ''),
    'conversejs_xmpp_password' => variable_get('conversejs_xmpp_password', ''),
    'conversejs_xmpp_domain' => variable_get('conversejs_xmpp_domain', ''),
    'conversejs_xmpp_resource' => variable_get('conversejs_xmpp_resource', 'ConverseJS'),
    'conversejs_param_fullname' => variable_get('conversejs_param_fullname', ''),
    'conversejs_visibility' => variable_get('conversejs_visibility', 0),
    'conversejs_pages' => variable_get('conversejs_pages', "admin\nadmin/*"),
    'conversejs_param_allow_contact_requests' => variable_get('conversejs_param_allow_contact_requests', 1),
    'conversejs_param_allow_muc' => variable_get('conversejs_param_allow_muc', 1),
    'conversejs_param_animate' => variable_get('conversejs_param_animate', 1),
    'conversejs_param_auto_list_rooms' => variable_get('conversejs_param_auto_list_rooms', 0),
    'conversejs_param_auto_reconnect' => variable_get('conversejs_param_auto_reconnect', 1),
    'conversejs_param_auto_subscribe' => variable_get('conversejs_param_auto_subscribe', 0),
    'conversejs_param_allow_otr' => variable_get('conversejs_param_allow_otr', 1),
    'conversejs_param_use_otr_by_default' => variable_get('conversejs_param_use_otr_by_default', 0),
    'conversejs_param_cache_otr_key' => variable_get('conversejs_param_cache_otr_key', 0),
    'conversejs_param_debug' => variable_get('conversejs_param_debug', 0),
    'conversejs_param_expose_rid_and_sid' => variable_get('conversejs_param_expose_rid_and_sid', 0),
    'conversejs_param_hide_muc_server' => variable_get('conversejs_param_hide_muc_server', 1),
    'conversejs_param_i18n_locale' => variable_get('conversejs_param_i18n_locale', 'en'),
    'conversejs_param_show_controlbox_by_default' => variable_get('conversejs_param_show_controlbox_by_default', 0),
    'conversejs_param_show_call_button' => variable_get('conversejs_param_show_call_button', 0),
    'conversejs_param_show_only_online_users' => variable_get('conversejs_param_show_only_online_users', 0),
    'conversejs_param_use_vcards' => variable_get('conversejs_param_use_vcards', 1),
    'conversejs_param_xhr_custom_status' => variable_get('conversejs_param_xhr_custom_status', 0),
    'conversejs_param_xhr_custom_status_url' => variable_get('conversejs_param_xhr_custom_status_url', ''),
    'conversejs_param_xhr_user_search' => variable_get('conversejs_param_xhr_user_search', 0),
    'conversejs_param_xhr_user_search_url' => variable_get('conversejs_param_xhr_user_search_url', ''),
    // Converse.js libraries and dependencies.
    'conversejs_libraries_unpacked' => variable_get('conversejs_libraries_unpacked', conversejs_library_paths(CONVERSEJS_UNPACKED)),
    'conversejs_libraries_packed' => variable_get('conversejs_libraries_packed', conversejs_library_paths(CONVERSEJS_PACKED)),
    'conversejs_libraries_mode' => variable_get('conversejs_libraries_mode', CONVERSEJS_PACKED),
  );

  if ($prepare) {
    // Prepare variables for actual use and resolve tokens
    // in fields which use tokens.
    // Replace tokens in fields which support tokens.
    $token_data = array(
      'user' => $user,
    );
    $params['conversejs_bosh_server'] = token_replace($params['conversejs_bosh_server'], $token_data);
    $params['conversejs_xmpp_user'] = token_replace($params['conversejs_xmpp_user'], $token_data);
    $params['conversejs_xmpp_password'] = token_replace($params['conversejs_xmpp_password'], $token_data);
    $params['conversejs_xmpp_domain'] = token_replace($params['conversejs_xmpp_domain'], $token_data);
    $params['conversejs_xmpp_resource'] = token_replace($params['conversejs_xmpp_resource'], $token_data);
    $params['conversejs_param_fullname'] = token_replace($params['conversejs_param_fullname'], $token_data);

    // Proxy address should be an absolute URI.
    $params['conversejs_proxy_address'] = file_create_url($params['conversejs_proxy_address']);

    // Auto-login feature should only be available
    // if user has permission to use it.
    $params['conversejs_autologin'] = $params['conversejs_autologin'] && user_access('autologin in conversejs');
  }

  if ($param_name) {
    return isset($params[$param_name]) ? $params[$param_name] : FALSE;
  }
  else {
    return $params;
  }
}

/**
 * Get default library paths.
 */
function conversejs_library_paths($mode = CONVERSEJS_PACKED) {
  switch ($mode) {
    case CONVERSEJS_UNPACKED:
      $paths = '
        components/otr/build/dep/salsa20.js
        components/otr/build/dep/bigint.js
        components/otr/vendor/cryptojs/core.js
        components/otr/vendor/cryptojs/enc-base64.js
        components/crypto-js-evanvosberg/src/md5.js
        components/crypto-js-evanvosberg/src/evpkdf.js
        components/otr/vendor/cryptojs/cipher-core.js
        components/otr/vendor/cryptojs/aes.js
        components/otr/vendor/cryptojs/sha1.js
        components/otr/vendor/cryptojs/sha256.js
        components/otr/vendor/cryptojs/hmac.js
        components/otr/vendor/cryptojs/pad-nopadding.js
        components/otr/vendor/cryptojs/mode-ctr.js
        components/otr/build/dep/eventemitter.js
        components/otr/build/otr.js
        components/strophe/strophe.js
        components/strophe.roster/index.js
        components/strophe.muc/index.js
        components/strophe.vcard/index.js
        components/strophe.disco/index.js
        components/underscore/underscore.js
        components/backbone/backbone.js
        components/backbone.localStorage/backbone.localStorage.js
        components/tinysort/src/jquery.tinysort.js
        components/jed/jed.js
        locale/en/LC_MESSAGES/en.js
        converse.js
        ';
      break;

    case CONVERSEJS_PACKED:
    default:
      $paths = '
        components_min/otr/build/dep/salsa20-min.js
        components_min/otr/build/dep/bigint-min.js
        components_min/otr/vendor/cryptojs/core-min.js
        components_min/otr/vendor/cryptojs/enc-base64-min.js
        components_min/crypto-js-evanvosberg/src/md5-min.js
        components_min/crypto-js-evanvosberg/src/evpkdf-min.js
        components_min/otr/vendor/cryptojs/cipher-core-min.js
        components_min/otr/vendor/cryptojs/aes-min.js
        components_min/otr/vendor/cryptojs/sha1-min.js
        components_min/otr/vendor/cryptojs/sha256-min.js
        components_min/otr/vendor/cryptojs/hmac-min.js
        components_min/otr/vendor/cryptojs/pad-nopadding-min.js
        components_min/otr/vendor/cryptojs/mode-ctr-min.js
        components_min/otr/build/dep/eventemitter-min.js
        components_min/otr/build/otr-min.js
        components_min/strophe/strophe-min.js
        components_min/strophe.roster/index-min.js
        components_min/strophe.muc/index-min.js
        components_min/strophe.vcard/index-min.js
        components_min/strophe.disco/index-min.js
        components_min/underscore/underscore-min.js
        components_min/backbone/backbone-min.js
        components_min/backbone.localStorage/backbone.localStorage-min.js
        components_min/tinysort/src/jquery.tinysort-min.js
        components_min/jed/jed-min.js
        locale/en/LC_MESSAGES/en.js
        converse-min.js
      ';
      break;

  }

  $path_prefix = drupal_get_path('module', 'conversejs') . '/lib/converse.js/';
  $result = '';

  $paths = trim(str_replace(' ', '', $paths));
  $paths_arr = explode("\n", $paths);
  foreach ($paths_arr as $path) {
    $path1 = trim($path);
    $path1 = $path_prefix . $path1;
    $result .= "\r\n{$path1}";
  }

  return trim($result);
}

/**
 * Add converse.js libraries to page according to settings.
 */
function conversejs_add_javascript_libraries() {
  $libraries_mode = conversejs_get_params('conversejs_libraries_mode', TRUE);
  $conversejs_libraries = '';

  switch ($libraries_mode) {
    case CONVERSEJS_UNPACKED:
      $conversejs_libraries = conversejs_get_params('conversejs_libraries_unpacked', TRUE);
      break;

    case CONVERSEJS_PACKED:
      $conversejs_libraries = conversejs_get_params('conversejs_libraries_packed', TRUE);
      break;

  }

  $paths_arr = explode("\n", $conversejs_libraries);
  foreach ($paths_arr as $path) {
    drupal_add_js(trim($path));
  }
}
